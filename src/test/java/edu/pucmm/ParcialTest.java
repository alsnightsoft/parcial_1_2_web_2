package edu.pucmm;

import org.jsoup.Jsoup;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;

/**
 * Documentación en http://www.vogella.com/tutorials/AndroidTesting/article.html
 * <p>
 * Created by aluis on 8/19/15.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ParcialTest {

    private static final String code = "112233";

    @Test
    public void getTest() {
        try {
            String result = Jsoup.connect("http://127.0.0.1:8070/exam/" + code).get().text();
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void postTest() {
        try {
            String result = Jsoup.connect("http://127.0.0.1:8070/exam").data("code", code).post().text();
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
