package edu.pucmm;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Documentación en http://www.vogella.com/tutorials/AndroidTesting/article.html
 * <p>
 * Created by aluis on 8/19/15.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CheckTest {

    @Test
    public void test() {
        assertEquals("Chequeo si 2 + 2 = 4?", 4, 2 + 2);
    }

    @Test
    public void testFail() {
        assertNotEquals("Chequeo si 2 + 2 = 5?", 5, 2 + 2);
    }
}
