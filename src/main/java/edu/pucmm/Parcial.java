package edu.pucmm;

import edu.pucmm.routes.ExamGET;
import edu.pucmm.routes.ExamPOST;
import spark.Spark;

public class Parcial {

    public static void main(String[] args) {
        try {
            Spark.port(8070);
            Spark.get("/exam/:code", new ExamGET());
            Spark.post("/exam", new ExamPOST());
            Spark.init();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
