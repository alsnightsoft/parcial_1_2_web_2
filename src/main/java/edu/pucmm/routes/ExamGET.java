package edu.pucmm.routes;

import edu.pucmm.domains.Student;
import edu.pucmm.utils.FileReader;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;
import java.util.List;

public class ExamGET implements Route {

    private static final FileReader FILE_READER = new FileReader();

    @Override
    public Object handle(Request request, Response response) throws Exception {
        printSeparator();
        String studentCode = request.params(":code");
        System.out.println("Code: " + studentCode);
        System.out.println("Agent: " + request.userAgent());
        System.out.println("Host: " + request.host());
        System.out.println("IP: " + request.ip());
        System.out.println("Port: " + request.port());
        if (studentCode != null) {
            Student studentFound = null;
            for (Student student : all()) {
                if (student.getCode().equals(studentCode)) {
                    studentFound = student;
                    break;
                }
            }
            if (studentFound != null) {
                System.out.println("Student: " + studentFound.toString());
                return "Felicidades!, tienes tus puntos, pero intentalo por post a ver que te dice!, recuerda que el post envia la variable en el cuerpo y no en la URL!";
            }
            return "Al parecer no sabes tu codigo y debes hablar con el profesor para obtenerlo o no sabes enviar el codigo y debes arreglarlo en tu código";
        } else {
            studentCode = "";
        }
        System.out.println("Error?! " + studentCode);
        return "-1";
    }

    private static void printSeparator() {
        System.out.println("------------------------------------------------");
    }

    private static List<Student> all() {
        List<Student> students = new ArrayList<>();
        for (String studentRow : FILE_READER.readFileStudents()) {
            Student student = new Student();
            student.transform(studentRow);
            students.add(student);
        }
        return students;
    }
}
