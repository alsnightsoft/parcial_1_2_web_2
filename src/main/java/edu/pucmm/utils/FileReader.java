package edu.pucmm.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class FileReader {

    public List<String> readFileStudents() {
        try {
            return Files.readAllLines(new File("StudentCodes.txt").toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
