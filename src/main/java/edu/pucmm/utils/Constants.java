package edu.pucmm.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Modelo java por rendimiento general
 * <p>
 * Created by aluis on 8/6/16.
 */
public class Constants {

    private static Constants instance = null;

    public static final String SEPARATOR = "|";

    private final String DATE_HOUR_FORMAT = "MM-dd-yyyy HH:mm:ss";

    private final Gson gson = new GsonBuilder().setDateFormat(DATE_HOUR_FORMAT).create();
    private final Gson gsonPretty = new GsonBuilder().setDateFormat(DATE_HOUR_FORMAT).setPrettyPrinting().create();

    private Constants() {
    }

    public static synchronized Constants get() {
        if (instance == null) {
            instance = new Constants();
        }
        return instance;
    }

    public String stringify(Object object) {
        return gson.toJson(object);
    }

    public String stringifyPretty(Object object) {
        return gsonPretty.toJson(object);
    }

    public final <T> T convert(String data, Class<T> validClass) {
        if (!data.isEmpty()) {
            try {
                return gson.fromJson(data, validClass);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }
        return null;
    }
}
