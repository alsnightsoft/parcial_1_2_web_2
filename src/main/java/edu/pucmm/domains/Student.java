package edu.pucmm.domains;

import edu.pucmm.utils.Constants;

import java.util.Objects;

public class Student {

    private String code;
    private String names;
    private String lastnames;
    private String warrant; // Matricula en este caso

    public Student() {
    }

    public Student(String code, String names, String lastnames) {
        this.code = code;
        this.names = names;
        this.lastnames = lastnames;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastnames() {
        return lastnames;
    }

    public void setLastnames(String lastnames) {
        this.lastnames = lastnames;
    }

    public String getWarrant() {
        return warrant;
    }

    public void setWarrant(String warrant) {
        this.warrant = warrant;
    }

    public void transform(String student) {
        String[] split = student.split("\\|");
        code = split[0];
        names = split[1];
        lastnames = split[2];
        warrant = split[3];
    }

    public String toRow() {
        return code + Constants.SEPARATOR +
                names + Constants.SEPARATOR +
                lastnames + Constants.SEPARATOR + warrant;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o.getClass().equals(this.getClass()) && Objects.equals(this.code, ((Student) o).code);
    }

    @Override
    public int hashCode() {
        if (code != null) {
            return code.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Code: " + code + " -- Estudiante: " + names + " " + lastnames + " -- " + warrant;
    }
}
